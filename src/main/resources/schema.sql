CREATE TABLE public.user (
  id INTEGER,
  name VARCHAR(150),
  email  VARCHAR(150),
  password VARCHAR(30),
  token VARCHAR(500),
  PRIMARY KEY (id)
);

CREATE TABLE public.phone (
  id INTEGER,
  ddd VARCHAR(3),
  number  VARCHAR(9),
  user_id INTEGER,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES public.user(id)
);
