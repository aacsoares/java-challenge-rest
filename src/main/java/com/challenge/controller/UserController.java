package com.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.exception.DefaultException;
import com.challenge.resource.ErrorResponseResource;
import com.challenge.resource.LoginParamResource;
import com.challenge.resource.UserParamResource;
import com.challenge.resource.UserResponseResource;
import com.challenge.service.UserService;

/**
 * Spring REST Controller that exposes the endpoints for consuming
 *
 * @author aaugusto
 *
 */
@RestController("user")
public class UserController {
    
    @Autowired
    private UserService userService;

    /**
     * Create User service
     *
     * @param userParam
     * @return The user created
     * @throws DefaultException
     */
    @RequestMapping(method = RequestMethod.POST, value = "/createUser", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserResponseResource cadastroUsuario(@RequestBody UserParamResource userParam) throws DefaultException {
        return userService.create(userParam);
    }

    /**
     * User Login
     *
     * @param loginParam
     * @return The User information
     * @throws DefaultException
     */
    @RequestMapping(method = RequestMethod.POST, value = "/login", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponseResource> login(@RequestBody LoginParamResource loginParam)
            throws DefaultException {
        return new ResponseEntity<UserResponseResource>(userService.login(loginParam), HttpStatus.OK);
    }
    
    /**
     * Service that returns the profile information for the user
     *
     * @param userId
     * @param token
     *            Used as Header parameter with Authorization tag
     * @return The user information
     * @throws DefaultException
     */
    @RequestMapping(method = RequestMethod.GET, value = "/userProfile/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserResponseResource userProfile(@PathVariable(value = "userId") String userId,
            @RequestHeader("Authorization") String token) throws DefaultException {
        return userService.getUserProfile(userId, token);
    }

    /**
     * Exception handler
     *
     * @param ex
     * @return A response entity representing the Error
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponseResource> exceptionHandler(Exception ex) {
        ErrorResponseResource error = new ErrorResponseResource();
        error.setMessage(ex.getMessage());
        if (ex instanceof DefaultException) {
            return new ResponseEntity<ErrorResponseResource>(error, ((DefaultException) ex).getStatusHttp());
        }
        return new ResponseEntity<ErrorResponseResource>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
