package com.challenge.exception;

import org.springframework.http.HttpStatus;

import com.challenge.enumeration.ErrorEnum;

/**
 * Default exception that is handled by all services
 *
 * @author aaugusto
 *
 */
public class DefaultException extends RuntimeException {
    
    private static final long serialVersionUID = -8022035226681839704L;
    private ErrorEnum error;

    public DefaultException(ErrorEnum error) {
        this.error = error;
    }

    @Override
    public String getMessage() {
        return error.getMessage();
    }

    public HttpStatus getStatusHttp() {
        return error.getCode();
    }
}
