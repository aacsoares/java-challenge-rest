package com.challenge.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

/**
 * Class responsible in creating the embedded database and load the schema
 * script
 * 
 * @author aaugusto
 *
 */
@Configuration
public class DataSourceConfig {
    
    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        EmbeddedDatabase db = builder.setName("java-challenge-db").setType(EmbeddedDatabaseType.HSQL).build();
        return db;
    }

}
