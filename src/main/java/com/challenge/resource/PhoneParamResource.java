package com.challenge.resource;

public class PhoneParamResource {
    
    public PhoneParamResource() {
    }
    
    public PhoneParamResource(String ddd, String number) {
        this.ddd = ddd;
        this.number = number;
    }

    private String ddd;
    private String number;
    
    public String getDdd() {
        return ddd;
    }
    
    public void setDdd(String ddd) {
        this.ddd = ddd;
    }
    
    public String getNumber() {
        return number;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
}