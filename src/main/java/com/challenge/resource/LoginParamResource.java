package com.challenge.resource;

import java.io.Serializable;

public class LoginParamResource implements Serializable {
    
    private static final long serialVersionUID = -5202181577955572432L;

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
