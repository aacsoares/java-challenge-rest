package com.challenge.service;

import com.challenge.exception.DefaultException;
import com.challenge.resource.LoginParamResource;
import com.challenge.resource.UserParamResource;
import com.challenge.resource.UserResponseResource;

/**
 * Service that represents the User actions
 *
 * @author aaugusto
 *
 */
public interface UserService {
    
    /**
     * Method that creates a new user in the database
     *
     * @param userParam
     * @return User registered
     * @throws DefaultException
     */
    UserResponseResource create(UserParamResource userParam) throws DefaultException;

    /**
     * Method that executes a login action in the database
     *
     * @param loginParam
     * @return User information
     * @throws DefaultException
     */
    UserResponseResource login(LoginParamResource loginParam) throws DefaultException;

    /**
     * Method that returns information about the user
     *
     * @param userId
     * @param token
     * @return User information
     * @throws DefaultException
     */
    UserResponseResource getUserProfile(String userId, String token) throws DefaultException;

}