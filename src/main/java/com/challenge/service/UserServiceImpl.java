package com.challenge.service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.entity.Phone;
import com.challenge.entity.User;
import com.challenge.enumeration.ErrorEnum;
import com.challenge.exception.DefaultException;
import com.challenge.repository.UserRepository;
import com.challenge.resource.LoginParamResource;
import com.challenge.resource.PhoneParamResource;
import com.challenge.resource.UserParamResource;
import com.challenge.resource.UserResponseResource;
import com.challenge.util.CryptoUtil;
import com.challenge.util.TokenUtil;

/**
 * {@inheritDoc}
 *
 * @author aaugusto
 *
 */
@Service
public class UserServiceImpl implements UserService {
    
    @Autowired
    private UserRepository userRepository;

    /**
     * {@inheritDoc}
     *
     * @see com.challenge.service.UserService#create(com.challenge.resource.
     *      UserParamResource)
     */
    @Override
    public UserResponseResource create(final UserParamResource userParam) throws DefaultException {
        UserResponseResource userCreated = new UserResponseResource();

        List<User> users = userRepository.findUserByEmail(userParam.getEmail());
        if ((users != null) && !users.isEmpty()) {
            throw new DefaultException(ErrorEnum.EMAIL_EXISTENTE);
        } else {
            User user = buildUserEntity(userParam);
            User userReturned = userRepository.save(user);
            buildUserResponse(userCreated, userReturned);
        }
        return userCreated;
    }

    private void buildUserResponse(UserResponseResource userCreated, User userReturned) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        userCreated.setCreateDate(userReturned.getCreateDate() != null ? sdf.format(userReturned.getCreateDate())
                : sdf.format(new Date()));
        userCreated.setLastLogin(
                userReturned.getLastLogin() != null ? sdf.format(userReturned.getLastLogin()) : sdf.format(new Date()));
        userCreated.setModifiedDate(userReturned.getLastModified() != null ? sdf.format(userReturned.getLastModified())
                : sdf.format(new Date()));
        userCreated.setId(userReturned.getId());
        userCreated.setToken(userReturned.getToken());
    }

    private User buildUserEntity(final UserParamResource userParam) {
        User user = new User();
        user.setName(userParam.getName());
        user.setEmail(userParam.getEmail());
        user.setPassword(CryptoUtil.generateHash(userParam.getPassword()));
        user.setPhones(new ArrayList<>());
        user.setCreateDate(new Date());
        user.setLastLogin(new Date());
        user.setLastModified(new Date());
        user.setToken(TokenUtil.generateToken(user.getEmail()));
        for (PhoneParamResource phoneParam : userParam.getPhones()) {
            Phone phone = new Phone();
            phone.setDdd(phoneParam.getDdd());
            phone.setNumber(phoneParam.getNumber());
            user.getPhones().add(phone);
        }
        return user;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.challenge.service.UserService#login(com.challenge.resource.
     *      LoginParamResource)
     */
    @Override
    public UserResponseResource login(LoginParamResource loginParam) throws DefaultException {
        UserResponseResource userCreated = new UserResponseResource();
        List<User> users = userRepository.findUserByEmail(loginParam.getEmail());
        if ((users != null) && !users.isEmpty()) {
            User userAux = users.get(0);
            System.out.println(userAux.getPassword());
            System.out.println(loginParam.getPassword());
            String hashLoginPassword = CryptoUtil.generateHash(loginParam.getPassword());
            System.out.println("Login hash: " + hashLoginPassword);
            if (userAux.getPassword().equals(hashLoginPassword)) {
                userAux.setLastLogin(new Date());
                userRepository.save(userAux);
                buildUserResponse(userCreated, userAux);
            } else {
                throw new DefaultException(ErrorEnum.USUARIO_SENHA_INVALIDO);
            }
        } else {
            throw new DefaultException(ErrorEnum.USUARIO_SENHA_INVALIDO);
        }
        
        return userCreated;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.challenge.service.UserService#getUserProfile(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public UserResponseResource getUserProfile(String userId, String token) throws DefaultException {
        UserResponseResource userCreated = new UserResponseResource();
        if ((token == null) || token.isEmpty()) {
            throw new DefaultException(ErrorEnum.NAO_AUTORIZADO);
        }
        User user = userRepository.findOne(Long.valueOf(userId));
        if (user != null) {
            if (token.equals(user.getToken())) {
                LocalDateTime currentDateTime = LocalDateTime.now();
                LocalDateTime lastLoginDateTime = LocalDateTime.ofInstant(user.getLastLogin().toInstant(),
                        ZoneId.systemDefault());
                Long minutes = ChronoUnit.MINUTES.between(lastLoginDateTime, currentDateTime);
                if (minutes >= 30L) {
                    throw new DefaultException(ErrorEnum.SESSAO_INVALIDA);
                } else {
                    buildUserResponse(userCreated, user);
                }
            } else {
                throw new DefaultException(ErrorEnum.NAO_AUTORIZADO);
            }
        } else {
            throw new DefaultException(ErrorEnum.USUARIO_INEXISTENTE);
        }
        return userCreated;
    }

}
