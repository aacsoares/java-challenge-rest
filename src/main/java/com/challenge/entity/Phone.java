package com.challenge.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "phone")
public class Phone implements Serializable {
    
    private static final long serialVersionUID = -8358815689209682936L;

    @Id
    @GeneratedValue
    private Long id;
    private String ddd;
    private String number;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    
    public Phone() {
    }

    public Phone(String ddd, String number) {
        this.ddd = ddd;
        this.number = number;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
