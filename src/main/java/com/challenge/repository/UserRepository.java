package com.challenge.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.challenge.entity.User;

/**
 * Repository that represents the User entity
 *
 * @author aaugusto
 *
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    
    /**
     * Method that returns a user by email
     *
     * @param email
     * @return The user
     */
    List<User> findUserByEmail(@Param("email") String email);

}
