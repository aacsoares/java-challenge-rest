package com.challenge.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.challenge.entity.Phone;

/**
 * Repository that represents the Phone entity
 *
 * @author aaugusto
 *
 */
@Repository
public interface PhoneRepository extends CrudRepository<Phone, Long> {
    
}
