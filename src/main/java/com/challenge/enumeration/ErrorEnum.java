package com.challenge.enumeration;

import org.springframework.http.HttpStatus;

/**
 * Enum responsible in mapping the errors and HTTP Status
 *
 * @author aaugusto
 *
 */
public enum ErrorEnum {
    
    NAO_AUTORIZADO("Não Autorizado", HttpStatus.UNAUTHORIZED), USUARIO_SENHA_INVALIDO("Usuário e/ou senha inválidos!",
            HttpStatus.UNAUTHORIZED), EMAIL_EXISTENTE("E-mail já existe!", HttpStatus.OK), SESSAO_INVALIDA(
                    "Sessão Inválida!",
                    HttpStatus.UNAUTHORIZED), USUARIO_INEXISTENTE("Usuário inexistente!", HttpStatus.OK);
    
    private String message;
    private HttpStatus code;
    
    ErrorEnum(String message, HttpStatus code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getCode() {
        return code;
    }
    
}
