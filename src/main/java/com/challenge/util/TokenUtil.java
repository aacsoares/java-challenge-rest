package com.challenge.util;

import java.security.Key;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

public final class TokenUtil {
    
    public static Key generalKey = MacProvider.generateKey();
    
    public static final String generateToken(String email) {
        if (email != null) {
            return Jwts.builder().setSubject(email).signWith(SignatureAlgorithm.HS512, generalKey).compact();
        } else {
            return "";
        }
    }
}
