package com.challenge.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.challenge.util.TokenUtil;

@RunWith(SpringRunner.class)
public class TokenUtilTestCase {
    
    @Test
    public final void testGenerateToken() {
        String result = TokenUtil.generateToken("teste@teste.com");
        assertNotNull(result);
        assertThat(result.length(), Matchers.greaterThan(0));
    }
    
    @Test
    public final void testGenerateTokenNullValues() {
        String result = TokenUtil.generateToken(null);
        assertThat(result, Matchers.isEmptyOrNullString());
    }
    
}
