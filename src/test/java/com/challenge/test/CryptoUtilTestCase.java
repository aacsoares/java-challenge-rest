package com.challenge.test;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.challenge.util.CryptoUtil;

@RunWith(SpringRunner.class)
public class CryptoUtilTestCase {
    
    @Test
    public final void testGenerateHash() {
        String result = CryptoUtil.generateHash("123456");
        assertThat(result.length(), Matchers.greaterThan(0));
        assertThat(result, Matchers.equalTo(
                "ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413"));
    }

    @Test
    public final void testGenerateHashNullValue() {
        String result = CryptoUtil.generateHash(null);
        assertThat(result, Matchers.isEmptyOrNullString());
    }

}
