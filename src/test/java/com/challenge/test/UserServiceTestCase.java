package com.challenge.test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import com.challenge.entity.Phone;
import com.challenge.entity.User;
import com.challenge.exception.DefaultException;
import com.challenge.repository.UserRepository;
import com.challenge.resource.LoginParamResource;
import com.challenge.resource.PhoneParamResource;
import com.challenge.resource.UserParamResource;
import com.challenge.resource.UserResponseResource;
import com.challenge.service.UserService;
import com.challenge.service.UserServiceImpl;

@RunWith(SpringRunner.class)
public class UserServiceTestCase {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Configuration
    static class TestConfiguration {
        
        @Bean
        public UserService userService() {
            return new UserServiceImpl();
        }

        @Bean
        public UserRepository userRepository() {
            return Mockito.mock(UserRepository.class);
        }
    }
    
    List<User> users = new ArrayList<>();
    
    @Before
    public void setUp() {
        users.add(createUserReturn(1L));
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(createUserReturn(2L));
    }

    private User createUserReturn(Long id) {
        User user = new User();
        user.setId(id);
        user.setEmail("teste@teste.com");
        user.setName("teste");
        user.setCreateDate(new Date());
        user.setLastLogin(new Date());
        user.setLastModified(new Date());
        user.setPassword(
                "ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413");
        user.setToken(
                "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0ZUB0ZXN0ZS5jb20ifQ.TenkY3a3yZHVWP4BoPSsDTGl4ORG_SVWK1Hph10jCnwLzb4LW0BuasYxnt86TxvZ8EC-M-nprF-V-AYQ7r7j7A");
        user.getPhones().add(new Phone("31", "12345678"));
        return user;
    }
    
    @Test
    public final void testCreate() {
        Mockito.when(userRepository.findUserByEmail("teste@teste.com")).thenReturn(null);
        UserParamResource userParam = new UserParamResource();
        userParam.setName("teste");
        userParam.setEmail("teste@teste.com");
        userParam.setPassword("123456");
        userParam.getPhones().add(new PhoneParamResource("31", "12345678"));
        UserResponseResource user = userService.create(userParam);
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
    }

    @Test
    public final void testCreateEmailJaExisteException() {
        exception.expect(DefaultException.class);
        exception.expectMessage(Matchers.containsString("E-mail já existe!"));

        Mockito.when(userRepository.findUserByEmail("teste@teste.com")).thenReturn(users);
        UserParamResource userParam = new UserParamResource();
        userParam.setName("teste");
        userParam.setEmail("teste@teste.com");
        userParam.setPassword("123456");
        userParam.getPhones().add(new PhoneParamResource("31", "12345678"));
        userService.create(userParam);
    }
    
    @Test
    public final void testLogin() {
        Mockito.when(userRepository.findUserByEmail("teste@teste.com")).thenReturn(users);
        LoginParamResource param = new LoginParamResource();
        param.setEmail("teste@teste.com");
        param.setPassword("123456");
        UserResponseResource user = userService.login(param);
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
    }

    @Test
    public final void testLoginUsuarioSenhaInvalidoException() {
        exception.expect(DefaultException.class);

        Mockito.when(userRepository.findUserByEmail("teste@teste.com")).thenReturn(users);
        LoginParamResource param = new LoginParamResource();
        param.setEmail("testetttt@teste.com");
        param.setPassword("123456");
        userService.login(param);
    }
    
    @Test
    public final void testGetUserProfile() {
        Mockito.when(userRepository.findOne(1L)).thenReturn(createUserReturn(1L));
        String userId = "1";
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0ZUB0ZXN0ZS5jb20ifQ.TenkY3a3yZHVWP4BoPSsDTGl4ORG_SVWK1Hph10jCnwLzb4LW0BuasYxnt86TxvZ8EC-M-nprF-V-AYQ7r7j7A";
        UserResponseResource user = userService.getUserProfile(userId, token);
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
    }

    @Test
    public final void testGetUserProfileComTokenVazioException() {
        exception.expect(DefaultException.class);
        exception.expectMessage(Matchers.containsString("Não Autorizado"));

        Mockito.when(userRepository.findOne(1L)).thenReturn(createUserReturn(1L));
        String userId = "1";
        String token = null;
        userService.getUserProfile(userId, token);
    }
    
    @Test
    public final void testGetUserProfileIdInvalidoException() {
        exception.expect(DefaultException.class);
        exception.expectMessage(Matchers.containsString("Usuário inexistente"));

        Mockito.when(userRepository.findOne(1L)).thenReturn(createUserReturn(1L));
        String userId = "10";
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0ZUB0ZXN0ZS5jb20ifQ.TenkY3a3yZHVWP4BoPSsDTGl4ORG_SVWK1Hph10jCnwLzb4LW0BuasYxnt86TxvZ8EC-M-nprF-V-AYQ7r7j7A";
        userService.getUserProfile(userId, token);
    }

    @Test
    public final void testGetUserProfileSessaoInvalidaException() {
        exception.expect(DefaultException.class);
        exception.expectMessage(Matchers.containsString("Sessão Inválida"));

        User createUserReturn = createUserReturn(1L);
        createUserReturn.setLastLogin(
                Date.from(LocalDateTime.now().minusMinutes(35).atZone(ZoneId.systemDefault()).toInstant()));
        Mockito.when(userRepository.findOne(1L)).thenReturn(createUserReturn);
        String userId = "1";
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0ZUB0ZXN0ZS5jb20ifQ.TenkY3a3yZHVWP4BoPSsDTGl4ORG_SVWK1Hph10jCnwLzb4LW0BuasYxnt86TxvZ8EC-M-nprF-V-AYQ7r7j7A";
        userService.getUserProfile(userId, token);
    }
    
}
