# README #

### Informações ###

Este repositório contém uma aplicação Spring Boot que expões 3 serviços:

 - Create user
 - Login
 - User Profile

### Acesso a aplicação ###

A aplicação está disponível em um ambiente de nuvem AWS e pode ser acessado no endereço:

 - Serviço Create User(POST): http://ec2-35-167-199-155.us-west-2.compute.amazonaws.com:8080/createUser
 - Serviço Login: http(POST): http//ec2-35-167-199-155.us-west-2.compute.amazonaws.com:8080/login
 - Serviço UserProfile(GET): http://ec2-35-167-199-155.us-west-2.compute.amazonaws.com:8080/userProfile/{userId}

### Utilização Local ###

Para utilização local, após realizar o clone do projeto executar a task bootRun do Gradle para executar a aplicação com Spring Boot. Para a execução dos testes unitários utilizat a task test do gradle.